// stolen from https://gist.github.com/549794
Drupal.behaviors.ie__7_select_width_fixer= function (context) {
  // bail early if we're not in a browser we need to fix
  //if (!$.browser.msie || $.browser.version > 7 ) { return; }
  
  $('select', context)
    .bind('click', function () {
      $(this).toggleClass('autowidth-clicked');
    } )
    .bind('focus mouseover', function () {
      // In some cases, dummy SELECT may prevent container from resizing on z-index change:
      $(this).addClass('autowidth-expand');
      $(this).removeClass('autowidth-clicked');
    } )
    .bind('mouseout', function () {
      if (!$(this).hasClass('autowidth-clicked')) {
        $(this).removeClass('autowidth-expand');
      }
    } )
    .bind('blur change', function () {
      $(this).removeClass('autowidth-expand autowidth-clicked');
    } );
}